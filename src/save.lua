--------------------------------------------------------------
-- 			Load and save functions --
--------------------------------------------------------------

local storage = minetest.get_mod_storage()
mcq_editor.datas = {}

function mcq_editor.save()
	for dataname,data in pairs(mcq_editor.datas) do
		storage:set_string(dataname,minetest.serialize(data))
	end
end

function mcq_editor.load()
--	local alldata = storage:to_table()
	mcq_editor.datas["question_data"] = minetest.deserialize(storage:get_string("question_data")) or {}
	mcq_editor.datas["player_data"] = minetest.deserialize(storage:get_string("player_data")) or {}
--	for dataname,data in pairs(alldata.fields) do
--		mcq_editor.datas[dataname] = minetest.deserialize(data) or {}
--	end
end