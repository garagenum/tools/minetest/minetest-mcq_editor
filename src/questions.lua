local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- load questions templates from .conf files
mcq_editor.register_question = function(question)
	local questions_dir = modpath.."/questions/"
	local textures_dir = modpath.."/textures/"
	local settings = Settings(questions_dir..question..".conf")
	local conf = settings:to_table()
	
	conf.name = conf.name or question
	
	if not conf.pic and mcq_editor.check_file(textures_dir..modname.."_pic_"..conf.name..".png") then
		conf.pic = modname.."_pic_"..conf.name..".png"
	end
	if not conf.form and mcq_editor.check_file(textures_dir..modname.."_form_"..conf.name..".png") then
		conf.form = modname.."_form_"..conf.name..".png"
	else conf.form = "default-image"
	end
	mcq_editor.datas["question_data"][question] = conf
	mcq_editor.save()
end

mcq_editor.register_questions = function()
	local questions_dir = modpath.."/questions/"
	local base_questions = minetest.get_dir_list(questions_dir,false)  -- false returns only file names
	for i,v in ipairs(base_questions) do
		if v:match("(%.conf)$") then
			local question,_ = v:match("(.-)(%.conf)$")
			mcq_editor.register_question(question)
		end
	end
end

