local modname = minetest.get_current_modname()

mcq_editor.unlock = function(name, question)
	-- Access Player Data
	local data = table.copy(mcq_editor.datas["player_data"][name])
	local qudef = mcq_editor.datas["question_data"][question]
	assert(qudef, "Unable to unlock a question which doesn't exist!")
	data.unlocked = data.unlocked or {}
	if data["unlocked"][question] and data["unlocked"][question] == question then
		return
	end
	
	-- Unlock question
	minetest.log("action", name.." has unlocked question "..question)
	data.unlocked[question] = question
	mcq_editor.datas["player_data"][name] = table.copy(data)
	mcq_editor.save()

	-- Give Prizes
	if qudef and qudef.prizes then
		for i = 1, #qudef.prizes do
			local itemstack = ItemStack(qudef.prizes[i])
			if not itemstack:is_empty() then
				local receiverref = minetest.get_player_by_name(name)
				if receiverref then
					receiverref:get_inventory():add_item("main", itemstack)
				end
			end
		end
	end
end

local good_answer_permanent_callback = nil
good_answer_permanent_callback = function(plname)
	print("good answer")
end

local bad_answer_permanent_callback = nil
bad_answer_permanent_callback = function(plname)
	print("bad answer")
end

local on_lock_callback = nil
on_lock_callback = function(plname,question)
	print(question.." locked")
end

local on_unlock_question = nil
on_unlock_question = function(plname,question)
	minetest.log("action", plname.." has unlocked question "..question)
	local qudef = mcq_editor.datas["question_data"][question]
	-- Give Prizes
	if qudef and qudef.prizes then
		for i = 1, #qudef.prizes do
			local itemstack = ItemStack(qudef.prizes[i])
			if not itemstack:is_empty() then
				local receiverref = minetest.get_player_by_name(plname)
				if receiverref then
					receiverref:get_inventory():add_item("main", itemstack)
				end
			end
		end
	end
end

local on_too_late_answer_callback = nil
on_too_late_answer_callback = function(plname,question)
	print("good answer but "..question.." already locked")
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
	--print("formname : "..formname)
-- Called when formname is one of defined questions
	local fname,question = string.match(formname,"^("..modname..":pic_)(.+)")
--	--print(fname)
	if fname and question then
		local plname = player:get_player_name()
		local qudef = table.copy(mcq_editor.datas["question_data"][question])
		mcq_editor.datas["player_data"][plname] = mcq_editor.datas["player_data"][plname] or {}
		local data = table.copy(mcq_editor.datas["player_data"][plname])
		data.locks = data.locks or {}
		data.attempts = data.attempts or {}
		data.attempts[question] = data.attempts[question] or 0
		data.unlocked = data.unlocked or {}
	--	--print(formname.." , la bonne réponse est : "..gnqcm.registered_questions[formname].reponses[reponse].." ("..reponse..")")
		print("fields : "..dump(fields))
		if qudef.limited then qudef.limited = tonumber(qudef.limited) end
		print("limited to "..qudef.limited.." 'type : "..type(qudef.limited))
		for i,_ in pairs(fields) do
			--[[ Test fields
	We will now test the fields received after user clicked on a button 
	inside a form , or press Escape, etc.
	Fields are table, we test indexes:
	it can match with reponse (aka good answer, defined with question)
	or with bad_answer, or just escape the form without callback
	--]]
			if i ~= qudef.reponse and i ~= "quit" then
				bad_answer_permanent_callback(plname)
			elseif i == qudef.reponse then
				good_answer_permanent_callback(plname)
			end
			print("attempts : "..data.attempts[question])
			if not qudef.limited or qudef.limited == 0 then
				if i ~= qudef.reponse and i ~= "quit" and not data.unlocked[question] then
					data.attempts[question] = data.attempts[question] + 1
				elseif i == qudef.reponse and not data.unlocked[question] then	
					data.unlocked[question] = question
					on_unlock_question(plname,question)
				end
			elseif qudef.limited and qudef.limited > 0 then
				if i ~= qudef.reponse and i ~= "quit" and not data.unlocked[question] then
					data.attempts[question] = data.attempts[question] + 1
					if data.attempts[question] == qudef.limited then
						data.locks[question] = question
						on_lock_callback(plname,question)
					end
				elseif i == qudef.reponse and not data.unlocked[question] then
					if data.locks[question] == question then
						on_too_late_answer_callback(plname,question)
					else 
						data.unlocked[question] = question
						on_unlock_question(plname,question)
					end
				end
			end
		end
		mcq_editor.datas["player_data"][plname] = table.copy(data)
		mcq_editor.save()
	end
end)
