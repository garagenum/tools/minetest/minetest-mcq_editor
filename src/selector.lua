-------------------------------------------------------------
-- Set of functions for creating selector pictures formspec --
--------------------------------------------------------------

-- Next functions intends to create a formspec letting you choose one 
-- or several pictures from textures folder, given a pattern prefix

mcq_editor.get_pic_list = function(path,prefix)
	local pic_list = minetest.get_dir_list(path,false)
	local messages = {}
	local patterndef
    print("we got a prefix : "..prefix)
	if prefix then
		patterndef = prefix..".+%.png" --or "^info_.+%.png"   -- fonctionnement à décrypter (todo)
	else patterndef = ".+%.png"
	end
	--print("pattern "..patterndef)
	--print("pictures dir : "..path)
	--print("pic list : "..dump(pic_list))
	for i,v in ipairs(pic_list) do
		if mcq_editor.check_file(path..v) and v:match(patterndef) then
			local _,message,_= v:match("^("..prefix..")(.*)(%.png)$")
			print(" message : "..message)
			table.insert(messages,message)
		end
	end
	--print("dump messages : "..dump(messages))
	return messages
end


local mk_formspec = nil
mk_formspec = function(path,prefix,messages)
	local fwidth = 15
	local fheight = 11
	local message_formspec = "size["..fwidth..","..fheight..",false]"..
		"no_prepend[]"..
		"label[2,0;Click on picture infos]"..
		"button_exit[13,0;2,1;clear;Clear]"
	local p,c = math.modf(math.sqrt(fheight))	
	for i,v in ipairs(messages) do
		local e,r = math.modf((i-1)/p)
		local xpos = e*p
	--	if i <= fheight-7 then
		local ypos = i-xpos
		ypos = ypos*2+(ypos-1) 				
		local length = #messages
		local f,s = math.modf((length-1)/p)
		xpos = xpos+(fwidth/2-(2*f-1)) 
		message_formspec = message_formspec.."image_button["..xpos..","..ypos..";2,2;"..prefix..v..".png;"..v..";]"
	end
	return message_formspec
end

function mcq_editor.make_formspec(folder,prefix)
	print("prefix is "..prefix.." and folder is "..folder.." at make_formspec call")
	local path = minetest.get_modpath("mcq_editor")
	print("path is "..path)
	if not path then 
		minetest.log("error","mod "..modname.." could not be loaded by mcq_editor")
		return
	else
		path = path.."/"..folder.."/"
		--minetest.mkdir(path)	
		local filenames = {}
		filenames = mcq_editor.get_pic_list(path,prefix)
		for _,v in ipairs(filenames) do
      		print("filename is "..v)
		end
		local formspec = mk_formspec(path,prefix,filenames)
		print("dump formspec "..dump(formspec))
		return formspec
	end
end


	-- Next functions register allpictures of a folder as wallmounted nodes 
	-- with name formed like: modname:basenodename_picturename
function mcq_editor.register_image_as_node(nodename,picturename)
	local modname = minetest.get_current_modname()
	local path = minetest.get_modpath(modname)
	if not path then 
		minetest.log("error","mod "..modname.." could not be loaded by gnapi")
		return
	else
		assert(type(nodename)  ==  "string")
		local completename
		if picturename then
			completename = nodename.."_"..picturename
		else
			completename = nodename
		end
		local node = modname..":"..completename
		print("newnode name : "..node)
		--print("completename : "..completename)
		minetest.register_node(node, {
			description = completename,
			drawtype = "signlike",
			tiles = {modname.."_"..completename..".png"},
			visual_scale = 1.0,
			inventory_image = modname.."_"..nodename..".png",
			wield_image = modname.."_"..nodename..".png",
			paramtype = "light",
			paramtype2 = "wallmounted",
			sunlight_propagates = true,
			walkable = false,
			selection_box = {
				type = "wallmounted"
			},
			groups = {oddly_breakable_by_hand=3, picture=1, not_in_craft_guide=1, not_in_creative_inventory=1},
		})
		print("new node registered : "..dump(node))
		return node
	end
end

local gnapi_can_dig = nil
local priv_for_dig = ""
gnapi_can_dig = function(pos,player)
	--print("privs : "..priv_for_dig)
	local plname = player:get_player_name()
	local keys=player:get_player_control()
	--print("keys[sneak] : "..dump(keys["sneak"]))
	local playerpriv = minetest.get_player_privs(plname)
	if playerpriv[priv_for_dig] == true and keys["sneak"] == true then
--	if (minetest.check_player_privs(plname,{priv_for_dig=true}) and keys["sneak"]==true) then
		--print("able to dig")
		return true
	else --print("not able to dig") return false
	end
end



function mcq_editor.register_images_as_nodes(folder,nodename,prefix,priv)
	--print("modname : "..modname)
	local modname = minetest.get_current_modname()
	local path = minetest.get_modpath(modname)
	if not path then 
		minetest.log("error","mod "..modname.." could not be loaded by gnapi")
		return
	else
		local nodes = {}
		local node = mcq_editor.register_image_as_node(nodename.."_")
		minetest.override_item(node,{groups = {oddly_breakable_by_hand=3, 
			picture=1, not_in_craft_guide=1} 
		})
		table.insert(nodes,node)
		
		local node_pic_path = path.."/".."textures".."/"
		path = path.."/"..folder
		local filenames = mcq_editor.get_pic_list(node_pic_path,prefix)
		for i,filename in ipairs(filenames) do
			local node = mcq_editor.register_image_as_node(nodename, filename)
			table.insert(nodes,node)
		end
		for i,node in ipairs(nodes) do
			print("nodes are : "..node.." with prefix "..prefix)
			if priv then
				priv_for_dig = priv
				minetest.override_item(node,{can_dig = gnapi_can_dig})
				minetest.override_item(node,
					{on_construct = function(pos) 
						--print("enter in on_construct rewrite ")
						local meta = minetest.get_meta(pos)
						local formspec = mcq_editor.make_formspec("textures", prefix)
						meta:set_string("formspec",formspec)
					end
				})
			end
		end
	end
end
