	-- Check if a file is readable
mcq_editor.check_file = function(filepath)
--local filename	= minetest.get_modpath("gnapi").."/textures/info_"..infoname..".png"
	local file		= io.open(filepath, "r")
	if file ~= nil then
		io.close(file)
		return true
	else return nil
	end
end	

	--  Round a pos to a 1-decimal value (for pos)
mcq_editor.round = {}
mcq_editor.round = function(table)
	local round_pos = {}
	for k,v in pairs(table) do
		round_pos[k]= math.ceil(v*100)/100
	end
	return round_pos
end
