local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

	-- mcq_editor.register_images_as_nodes(modname,folder,nodename,prefix,priv)
local folder = "questions"
local nodename = "pic"
local prefix = modname.."_"..nodename.."_"
local priv = "level"

mcq_editor.register_images_as_nodes(folder,nodename,prefix,priv)

	-- This is done because gnapi register a formspec in meta, 
	-- which lets user select a picture at node's pos 
local modname = minetest.get_current_modname()
local on_receive_mcq_fields = nil
on_receive_mcq_fields = function(pos,formname,fields,placer)
	for field,value in pairs(fields) do
		if field ~="quit" and field ~="clear" then
			local oldnode = minetest.get_node(pos)
			minetest.swap_node(pos,{name=modname..":pic_"..field,param2=oldnode.param2})
		elseif field == "clear" then
			local oldnode = minetest.get_node(pos)
			minetest.swap_node(pos,{name=modname..":pic_",param2=oldnode.param2})			
		end
	end
end

local make_question_formspec = nil
make_question_formspec = function(question)
	local indice = 0
	local questiondef= table.copy(mcq_editor.datas["question_data"][question])
	--print("dump question def : "..dump(questiondef))
	for i,v in pairs(questiondef) do
		if i == string.match(i,"^(reponse%d)") then
			indice = indice + 1	
		end
	end
	local buttons = {}
	local posx = 0
	-- il faudrait trouver un calcul général , avec modulo?
	local posx = math.floor(9/indice)-1
	for i=1,indice,1 do
		table.insert(buttons,"image_button_exit["..posx..",6;1,1;"..modname.."_"..i..".png;reponse"..i..";]")
		posx = posx+2
	end

	local formbuttons = ""
	for _,v in ipairs(buttons) do
		formbuttons = formbuttons..v
	end
	local formspec = ("size[9,7]"..
		"background[0,0;,;"..questiondef.form..";true]"..formbuttons)
	--print("question formspec : "..formspec)
	return formspec
end


local on_punch_mcq_node = nil
on_punch_mcq_node = function(pos,node,player,pointed_thing)
	local name = player:get_player_name()
	--print("node def : "..dump(minetest.registered_nodes[node.name]))
	--print("pos meta:"..dump(minetest.get_meta(pos):to_table()))
	local _,question = string.match(node.name, "^(mcq_editor:pic_)(.+)")
	--print("node name : "..node.name)
	if question then
	--	minetest.show_formspec(name,node.name.."_"..minetest.pos_to_string(pos), make_question_formspec(question))
		minetest.show_formspec(name,node.name, make_question_formspec(question))
	end
end


mcq_editor.custom_mcq_nodes = function()
	for nodename,nodedef in pairs(minetest.registered_nodes) do
		if nodename:match("^(mcq_editor:pic)") then
			minetest.override_item(nodename,{on_receive_fields = on_receive_mcq_fields})
			minetest.override_item(nodename,{on_punch = on_punch_mcq_node})
		end
	end
end
