mcq_editor = {}
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Use save/load functions from mod gnapi
dofile(modpath.."/src/save.lua")
mcq_editor.load()
dofile(modpath.."/src/selector.lua")
dofile(modpath.."/src/utils.lua")

dofile(modpath.."/src/questions.lua")
dofile(modpath.."/src/answers.lua")
mcq_editor.register_questions()

dofile(modpath.."/src/nodes.lua")
mcq_editor.custom_mcq_nodes()


